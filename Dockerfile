FROM ruby:alpine AS builder

RUN apk update && apk add g++ make && gem install package_cloud

FROM ruby:alpine

COPY --from=builder /usr/local/bundle /usr/local/bundle